//
//  main.m
//  pointers
//
//  Created by Yakup on 12.07.2017.
//  Copyright © 2017 Yakup. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        //Pointerler adres değeri tutan veri tipleridir.
        
        int k = 25;
        int *p,*t,*u;
        p=&k; //k'nın adresini tutacak olan p pointerina atama yapıyoruz.
        t = p +1;
        u = &k + 1;
        char x[5] = "Yakup";
        char *c;
        c=x;
        
        
        NSLog(@"k'nin adresi : %p", p);
        NSLog(@"k'nın Değeri: %i", *p);
        NSLog(@"k'nin adresine 1 eklenmiş hali: %p", t);
        NSLog(@"k'nin adresine 1 eklenmiş hali: %p", u);
        NSLog(@"x'in Başlangıç Adresi : %p", u);

        

        


        
        
        
    }
    return 0;
}
